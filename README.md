# Ordermentum Traffic Lights

## Quick Start

`npm install`

`npm start`  

To give startTime and endTime to the app, example below:

`npm start -- --startTime="2013-02-08 09:00" --endTime="2013-02-08 09:30"`
  
or run in serve mode:
  
`npm run serve` , or:

`npm run serve -- --startTime="2013-02-08 09:00" --endTime="2013-02-08 09:30"`

## Tests

`npm run test` to start all unit tests

**The last test contains large test data set, if you have problems or timeout running it, please increase the timeout field passed to mocha**

`npm run coverage` to run test coverage report