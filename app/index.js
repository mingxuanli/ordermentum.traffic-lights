var lights = require('./lights');
var moment = require('moment');
var argv = require('yargs').argv;

let startTime = moment(argv.startTime);
let endTime = moment(argv.endTime);

let changes = lights.getLightChanges(startTime, endTime);

console.log(changes);