var R = require('ramda');

const DIRECTION = {
    N: 'N',
    S: 'S',
    E: 'E',
    W: 'W'
};
const STATUS = {
    GREEN: 'GREEN',
    YELLOW: 'YELLOW',
    RED: 'RED'
}
const lightDuration = 5 * 60; // seconds
const yellowDuration = 30; // seconds
const interval = 'seconds';

/**
 * Function to return a list of light changes
 *
 * @param {Datetime} startTime - YYYY-MM-DD HH:mm:ss format
 * @param {Datetime} endTime - YYYY-MM-DD HH:mm:ss format
 * @return {Array} outputs
 *
 * */
function getLightChanges(startTime, endTime) {
    let outputs = [];
    let currentTime = startTime;
    let timeDiff = endTime.diff(startTime, interval);
    if (timeDiff < 0) {
        return outputs;
    } else {
        let numChanges = Math.ceil(timeDiff / (lightDuration + yellowDuration)) + 1;
        let changes = R.times(R.identity, numChanges);
        R.forEach((changeIndex) => {
            outputs = R.concat(outputs, getLightChange(currentTime, endTime, interval, changeIndex));
        }, changes);
        return outputs;
    }
};

/**
 * Function to return a list of light changes in one turn - (light from red to green) or (light from green to red)
 *
 * @param {Datetime} startTime - YYYY-MM-DD HH:mm:ss format
 * @param {Datetime} endTime - YYYY-MM-DD HH:mm:ss format
 * @param {String} interval
 * @param {Number} changeIndex
 * @return {Array} outputs
 *
 * */
function getLightChange(currentTime, endTime, interval, changeIndex) {
    let changes = [];
    let allDirections = [DIRECTION.N, DIRECTION.S, DIRECTION.E, DIRECTION.W];
    let yellowLightDirections = changeIndex % 2 === 0 ? [DIRECTION.N, DIRECTION.S] : [DIRECTION.E, DIRECTION.W];

    const addTime = (light) => {
        light.time = currentTime.format('MM/DD/YYYY HH:mm:ss');
        return light;
    };

    const isNorthOrSouth = (direction) => {
        return direction === DIRECTION.N || direction === DIRECTION.S;
    };

    const getGreenLight = (direction) => {
        return {
            direction : direction,
            status : changeIndex % 2 === 0 ? STATUS.GREEN : STATUS.RED
        };
    };

    const getRedLight = (direction) => {
        return {
            direction : direction,
            status : changeIndex % 2 === 0 ? STATUS.RED : STATUS.GREEN
        };
    };

    const getYellowLight = (direction) => {
        return {
            direction : direction,
            status : STATUS.YELLOW
        }
    };

    if (currentTime <= endTime) {
        let mapGreenOrRedLights = R.ifElse(
            isNorthOrSouth,
            getGreenLight,
            getRedLight
        );

        let greenOrRedLights = R.map(addTime, R.map(mapGreenOrRedLights, allDirections));
        changes = R.concat(changes, greenOrRedLights);
        currentTime.add(lightDuration, interval);
    }

    if (currentTime <= endTime) {
        let yellowLights = R.map(addTime, R.map(getYellowLight, yellowLightDirections));
        changes = R.concat(changes, yellowLights);
        currentTime.add(yellowDuration, interval);
    }

    return changes;
};

module.exports = {
    getLightChanges
};