const expect = require('chai').expect;
const moment = require('moment');
const lights = require('../app/lights');

describe('Traffic Lights - Unit Test', () => {

    describe('Simple Test - Same startTime and endTime', () => {
        let startTime;
        let endTime;

        before(() => {
            startTime = moment();
            endTime = moment();
        });

        it('Should output two change outputs', () => {
            let expectedOutput = [
                { direction: 'N', status: 'GREEN', time: startTime.format('MM/DD/YYYY HH:mm:ss') },
                { direction: 'S', status: 'GREEN', time: startTime.format('MM/DD/YYYY HH:mm:ss') },
                { direction: 'E', status: 'RED', time: startTime.format('MM/DD/YYYY HH:mm:ss') },
                { direction: 'W', status: 'RED', time: startTime.format('MM/DD/YYYY HH:mm:ss') }
            ];
            let actualOutputs = lights.getLightChanges(startTime, endTime);

            expect(expectedOutput.length).to.equal(actualOutputs.length);
            expect(expectedOutput).to.deep.equal(actualOutputs);
        });
    });

    describe('Invalid Time Test - endTime before startTime', () => {
        let startTime;
        let endTime;

        before(() => {
            startTime = moment();
            endTime = moment().subtract(1, 'seconds');
        });

        it('Should output empty outputs', () => {
            let expectedOutput = [];
            let actualOutputs = lights.getLightChanges(startTime, endTime);

            expect(expectedOutput.length).to.equal(actualOutputs.length);
            expect(expectedOutput).to.deep.equal(actualOutputs);
        });
    });

    describe('Boundary Condition Test - 1s diff between startTime and endTime', () => {
        let startTime;
        let endTime;

        before(() => {
            startTime = moment();
            endTime = moment().add(1, 'seconds');
        });

        it('Should output two outputs', () => {
            let expectedOutput = [
                { direction: 'N', status: 'GREEN', time: startTime.format('MM/DD/YYYY HH:mm:ss') },
                { direction: 'S', status: 'GREEN', time: startTime.format('MM/DD/YYYY HH:mm:ss') },
                { direction: 'E', status: 'RED', time: startTime.format('MM/DD/YYYY HH:mm:ss') },
                { direction: 'W', status: 'RED', time: startTime.format('MM/DD/YYYY HH:mm:ss') }
            ];
            let actualOutputs = lights.getLightChanges(startTime, endTime);

            expect(expectedOutput.length).to.equal(actualOutputs.length);
            expect(expectedOutput).to.deep.equal(actualOutputs);
        });
    });

    describe('Normal Time Test - 5 min interval', () => {
        let startTime;
        let endTime;

        before(() => {
            startTime = moment();
            endTime = moment().add(300, 'seconds');
        });

        it('Should output four outputs', () => {
            let now = startTime.clone();
            let expectedOutput = [
                { direction: 'N', status: 'GREEN', time: startTime.format('MM/DD/YYYY HH:mm:ss') },
                { direction: 'S', status: 'GREEN', time: startTime.format('MM/DD/YYYY HH:mm:ss') },
                { direction: 'E', status: 'RED', time: startTime.format('MM/DD/YYYY HH:mm:ss') },
                { direction: 'W', status: 'RED', time: startTime.format('MM/DD/YYYY HH:mm:ss') },
                { direction: 'N', status: 'YELLOW', time: now.add(300, 'seconds').format('MM/DD/YYYY HH:mm:ss') },
                { direction: 'S', status: 'YELLOW', time: now.format('MM/DD/YYYY HH:mm:ss')}
            ];
            let actualOutputs = lights.getLightChanges(startTime, endTime);

            expect(expectedOutput.length).to.equal(actualOutputs.length);
            expect(expectedOutput).to.deep.equal(actualOutputs);
        });
    });

    describe('Normal Time Test - 5 min 30 seconds interval', () => {
        let startTime;
        let endTime;

        before(() => {
            startTime = moment();
            endTime = moment().add(330, 'seconds');
        });

        it('Should output six outputs', () => {
            let now = startTime.clone();
            let expectedOutput = [
                { direction: 'N', status: 'GREEN', time: startTime.format('MM/DD/YYYY HH:mm:ss') },
                { direction: 'S', status: 'GREEN', time: startTime.format('MM/DD/YYYY HH:mm:ss') },
                { direction: 'E', status: 'RED', time: startTime.format('MM/DD/YYYY HH:mm:ss') },
                { direction: 'W', status: 'RED', time: startTime.format('MM/DD/YYYY HH:mm:ss') },
                { direction: 'N', status: 'YELLOW', time: now.add(300, 'seconds').format('MM/DD/YYYY HH:mm:ss') },
                { direction: 'S', status: 'YELLOW', time: now.format('MM/DD/YYYY HH:mm:ss')},
                { direction: 'N', status: 'RED', time: now.add(30, 'seconds').format('MM/DD/YYYY HH:mm:ss') },
                { direction: 'S', status: 'RED', time: now.format('MM/DD/YYYY HH:mm:ss') },
                { direction: 'E', status: 'GREEN', time: now.format('MM/DD/YYYY HH:mm:ss') },
                { direction: 'W', status: 'GREEN', time: now.format('MM/DD/YYYY HH:mm:ss') }
            ];
            let actualOutputs = lights.getLightChanges(startTime, endTime);

            expect(expectedOutput.length).to.equal(actualOutputs.length);
            expect(expectedOutput).to.deep.equal(actualOutputs);
        });
    });

    describe('Normal Time Test - Half an hour interval', () => {
        let startTime;
        let endTime;

        before(() => {
            startTime = moment('2013-02-08 09:00:00');
            endTime = moment('2013-02-08 09:30:00');
        });

        it('Should output a list of outputs', () => {
            let expectedOutput = require('./mock-half-hour.json');
            let actualOutputs = lights.getLightChanges(startTime, endTime);
            expect(expectedOutput.length).to.equal(actualOutputs.length);
            expect(expectedOutput).to.deep.equal(actualOutputs);
        });
    });

    describe('Big Time Test - 1 month interval', () => {
        let startTime;
        let endTime;

        before(() => {
            startTime = moment('2013-02-08 09:00:00');
            endTime = moment('2013-03-08 09:00:00');
        });

        it('Should output a list of large outputs', () => {
            let expectedOutput = require('./mock-one-month.json');
            let actualOutputs = lights.getLightChanges(startTime, endTime);
            expect(expectedOutput.length).to.equal(actualOutputs.length);
            expect(expectedOutput).to.deep.equal(actualOutputs);
        });
    });
});